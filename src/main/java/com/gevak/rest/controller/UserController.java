package com.gevak.rest.controller;

import com.gevak.rest.controller.dto.UserDto;
import com.gevak.rest.controller.dto.builder.UserDtoBuilder;
import com.gevak.rest.entity.User;
import com.gevak.rest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/users/{user_id}", produces = MediaType.APPLICATION_JSON)
    public UserDto getUser(@PathVariable("user_id") Long userId) {
        return UserDtoBuilder.toDto(userService.findOne(userId));
    }

    @PostMapping(value = "/users", produces = MediaType.APPLICATION_JSON)
    public UserDto createUser(@RequestBody UserDto userDto) {
        User user = UserDtoBuilder.toEntity(userDto);
        return UserDtoBuilder.toDto(userService.save(user));
    }
}
