package com.gevak.rest.controller.dto.builder;

import com.gevak.rest.controller.dto.UserDto;
import com.gevak.rest.entity.User;

public class UserDtoBuilder {

    public static UserDto toDto(User user){
        return new UserDto(user.getUserId(), user.getFirstName(), user.getLastName(), user.getAge());
    }

    public static User toEntity(UserDto userDto){
        return new User(userDto.firstName, userDto.lastName, userDto.age);
    }

}
