package com.gevak.rest.service;

import com.gevak.rest.entity.User;

public interface UserService {

    User findOne(Long userId);

    User save(User user);

}
