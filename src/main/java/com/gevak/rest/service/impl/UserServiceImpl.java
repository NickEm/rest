package com.gevak.rest.service.impl;

import com.gevak.rest.entity.User;
import com.gevak.rest.repository.UserRepository;
import com.gevak.rest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findOne(Long userId) {
        return userRepository.findByUserId(userId);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }
}
